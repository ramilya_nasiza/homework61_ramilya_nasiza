import React, {Component} from 'react';

import './CountryInfo.css';
import axios from "axios";

class CountryInfo extends Component {

  state = {
    oneCountry: [],
    borders: []
  };

  componentDidUpdate (prevProps) {
    if(this.props.codeCountry){
      if(prevProps.codeCountry !== this.props.codeCountry){
        axios.get('/alpha/' + this.props.codeCountry).then(response => {
          this.setState({oneCountry: response.data});
          return response.data;

        }).then(data =>{
          const bordersCountry = [];
          return Promise.all(data.borders.map(bord => {
            return axios.get('/alpha/'+ bord).then(response => {
              bordersCountry.push(response.data.name);
              this.setState({borders: bordersCountry})
            });
          }));
        })
      }
    }
  }

  render() {
    return (
        <div className='CountryInfo'>
          <h2>{this.state.oneCountry.name}</h2>
          <p className='capital'>Capital: <span>{this.state.oneCountry.capital}</span></p>
          <div className='border-with'>Borders with:
            <ul>
              {this.state.borders.map((border, index) =>{
                return <li key={index}>{border}</li>
              })}
            </ul>
          </div>
          <img src={this.state.oneCountry.flag} alt=""/>
        </div>
    );
  }
}

export default CountryInfo;