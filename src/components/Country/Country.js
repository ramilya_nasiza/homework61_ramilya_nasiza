import React, {Component} from 'react';

import './Country.css';


class Country extends Component {

  render() {
    return (
        <p onClick={this.props.clicked} className='country' key={this.props.alphacode}>{this.props.title}</p>
    );
  }
}

export default Country;