import React, {Component} from 'react';
import CountryInfo from "../../components/CountryInfo/CountryInfo";
import Country from "../../components/Country/Country";
import axios from "axios";

import './RestCounties.css';

class RestCountries extends Component {

  state = {
    countries: [],
    selectedCountry: null
  };

  componentDidMount (){
    const allCountriesUrl = '/all?fields=name;alpha3Code';
    axios.get(allCountriesUrl).then(response=>{
      return response.data;
    }).then(data => {
      this.setState({countries: data})
    }).catch(error => {
      console.log(error);
    })
  }

  countrySelectedHandler = code => {
    this.setState({selectedCountry: code});
  };

  render() {
    return (
        <div className='RestCountries'>
          <div className='countries'>
            {this.state.countries.map(country => (
                <Country
                    key={country.alpha3Code}
                    title={country.name}
                    alphacode={country.alpha3Code}
                    clicked={() => this.countrySelectedHandler(country.alpha3Code)}
                />
            ))}
          </div>
          <CountryInfo codeCountry={this.state.selectedCountry}/>
        </div>
    );
  }
}

export default RestCountries;