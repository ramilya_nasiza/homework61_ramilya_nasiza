import React, { Component } from 'react';
import RestCountries from "./container/RestCountries/RestCountries";

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <RestCountries/>
      </div>
    );
  }
}

export default App;
